import { Injectable } from '@angular/core';
import { DataSet } from '../models/data-set.model';
import { Data } from '../models/data.model';

@Injectable({
  providedIn: 'root'
})
export class DataTransformationService {

  constructor() { }

  public transform(scores: any): Data {
    let data = new Data();
    data.labels = scores.data.DAILY.dates
                        .filter((element: string) => element != null)
                        .map((element:string) => element.substring(0,4) + '-' + element.substring(4,6) + '-' + element.substring(6,8));
    let datasets = new DataSet();
    datasets.label = 'Nombre de points';
    datasets.data = this.aggregateScores(scores.data.DAILY.dataByMember, scores.data.DAILY.dates.length);
    for (let i = 0; i < data.labels.length; i++) {
      datasets.backgroundColor.push('rgba(201, 203, 207, 0.2)');
      datasets.borderColor.push('rgb(201, 203, 207)');
    }
    datasets.borderWidth = 1;
    data.datasets.push(datasets);
    return data;
  }

  private aggregateScores(scores, numberOfDays: number){
    let results : number[] = [];
    for(let i=0 ; i< numberOfDays ; i++){
      let result = 0;
      result = this.resultForDay(scores, i);
      if(result != -1) {
        results.push(result);
      }
    }
    return results;
  }

  resultForDay(scores, dayNumber: number) : number{
    let resultForTheDay = 0;
    let resultsForDayExists = false;
    for (const [key, value] of Object.entries(scores.players)) {
      if(scores.players[key].points[dayNumber] != null){
        resultForTheDay += scores.players[key].points[dayNumber];
        resultsForDayExists = true;
      }
    }
    if(resultsForDayExists){
      return resultForTheDay;
    } else {
      return -1
    }
  }
}
