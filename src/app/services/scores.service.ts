import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ScoresService {

  private apiURL = environment.apiURL;
  constructor(private http: HttpClient) { }

  getScores() {
    return this.http.get(this.apiURL + '/demo/api.json');
  }
}
