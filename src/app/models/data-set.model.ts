export class DataSet {
    public label: string = '';
    public data: number[] = [];
    public backgroundColor: string[] = [];
    public borderColor: string[] = [];
    public borderWidth: number  = 0;
}