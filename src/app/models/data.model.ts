import { DataSet } from "./data-set.model";

export class Data {
    public labels: string[] = [];
    public datasets: DataSet[] = [];
}