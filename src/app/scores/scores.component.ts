import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Chart, registerables, Tooltip } from 'chart.js';
import { DataTransformationService } from '../services/data-transformation.service';
import { ScoresService } from '../services/scores.service';

@Component({
  selector: 'app-scores',
  templateUrl: './scores.component.html',
  styleUrls: ['./scores.component.scss']
})
export class ScoresComponent implements OnInit {
  chart: any = [];
  toolTipX = 0;
  toolTipY = 0;
  constructor(private _router: Router, private route: ActivatedRoute, private dataTransformationService: DataTransformationService, private ref: ChangeDetectorRef, private scoresService: ScoresService) {
    Chart.register(...registerables);
  }

  ngOnInit(): void {
  
    this.route.queryParams.subscribe(params => {
      // console.log(params);
      // select the chart
    });

    this.scoresService.getScores().subscribe(scores => {
      this.chart =  new Chart('big-line-chart', {
        type: 'bar',
        data: this.dataTransformationService.transform(scores),
        options: {
          scales: {
            x: {
              stacked: true
            },
            y: {
              beginAtZero: true,
              stacked: true
            }
          },
          onClick:  (event, clickedElements) => {
            if(clickedElements.length != 0){
              this._router.navigate(['/scores'], {
                queryParams: {
                  date: this.getDateByIndex(clickedElements[0].index)
                },
                queryParamsHandling: 'merge',
              });
            }
          },
          events: ['click']
        }
      });
    },
      (error) => {
        console.error(error);
        // return new Data();
      });
  }
  getDateByIndex(index: number){
    return this.chart.config.data.labels[index];
  }
 
}
